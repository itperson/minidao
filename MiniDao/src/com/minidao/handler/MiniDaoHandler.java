package com.minidao.handler;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;


import com.minidao.annotation.Arguments;
import com.minidao.annotation.ResultType;
import com.minidao.freemarker.FreeMarkerFactory;

public class MiniDaoHandler implements InvocationHandler {
	private boolean showSql = false;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public Object invoke(Object proxy, Method method, Object[] args)
			throws Throwable {
		// TODO Auto-generated method stub
		Object ret = null;
		Map<String, Object> paramMap = new HashMap<String, Object>();
		getTemplateParam(paramMap, method, args);
		String executeSql = getTemplateSql(paramMap, method);
		if(showSql){
			System.out.println("preDao sql==>>"+executeSql);
		}

		ret = executeObject(method, executeSql);

		return ret;
	}
	
	public void getTemplateParam(Map<String, Object> sqlParamsMap, Method method, Object[] args) throws Exception {
		if (method.isAnnotationPresent(Arguments.class)&&args==null) {
			throw new Exception("没有定义@Arguments参数！");
		}
		
		if(method.isAnnotationPresent(Arguments.class)){
			Arguments annotation = method.getAnnotation(Arguments.class);
			if (annotation.value().length != args.length) {
				throw new Exception("参数个数与方法参数个数不一致，请检查配置！");
			}
			int index = 0;
			for (String paramerName : annotation.value()) {
				sqlParamsMap.put(paramerName, args[index]);
				index++;
			}
		}
	}
	
	private Object executeObject(Method method, String executeSql) {
		//判断是否是insert/update操作
		if(isUpIns(executeSql)){
			jdbcTemplate.execute(executeSql);
		}
		
		Class<?> returnType = method.getReturnType();
		if(returnType.isPrimitive()){
			Number number = (Number)jdbcTemplate.queryForObject(executeSql, returnType);
			String vlaueR = returnType.getCanonicalName();
			if("int".equals(vlaueR)){
				return number.intValue();
			}else if("double".equals(vlaueR)){
				return number.doubleValue();
			}else if("long".equals(vlaueR)){
				return number.longValue();
			}
		 }
		if(String.class==returnType){
			String stringR = (String)jdbcTemplate.queryForObject(executeSql, returnType);
			return stringR;
		}
		if (returnType == List.class && method.isAnnotationPresent(ResultType.class)) {
			ResultType rtype = method.getAnnotation(ResultType.class);
			return jdbcTemplate.query(executeSql,BeanPropertyRowMapper.newInstance(rtype.value()));
		}
		return null;
	}
	
	private boolean isUpIns(String executeSql) {
		 return (executeSql.startsWith("insert") || executeSql.startsWith("update") || executeSql.startsWith("delete")) ?true:false;
	}
	
	private String getTemplateSql(Map<String, Object> sqlParamsMap, Method method) {
		String sqlTpl = method.getDeclaringClass().getSimpleName() + "_" + method.getName() + ".sql";
		return FreeMarkerFactory.parseTemplate(sqlTpl, sqlParamsMap);
	}
	
	public boolean isShowSql() {
		return showSql;
	}

	public void setShowSql(boolean showSql) {
		this.showSql = showSql;
	}

}
