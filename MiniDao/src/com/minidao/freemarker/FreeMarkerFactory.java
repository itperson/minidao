package com.minidao.freemarker;

import java.io.StringWriter;
import java.util.Map;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class FreeMarkerFactory {
	private static final String ENCODE = "utf-8";
	private static final Configuration sqlConfig = new Configuration(
			Configuration.VERSION_2_3_25);

	static {
		sqlConfig.setClassForTemplateLoading(FreeMarkerFactory.class, "/");
	}
	
    public static boolean isExistTemplate(String tplName) {
        try {
            Template mytpl = sqlConfig.getTemplate(tplName, "UTF-8");
            if (mytpl == null) {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }
    
    public static String parseTemplate(String tplName, Map<String, Object> paras) {
        try {
            StringWriter swriter = new StringWriter();
            Template mytpl = sqlConfig.getTemplate(tplName, ENCODE);
            mytpl.process(paras, swriter);
            return swriter.toString();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println("模板名:{ "+ tplName +" }");
            throw new RuntimeException("解析SQL模板异常");
        }
    }
}
