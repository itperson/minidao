package com.minidao.factory;

import java.lang.annotation.Annotation;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.GenericBeanDefinition;

import com.minidao.annotation.MiniDao;
import com.minidao.handler.MiniDaoHandler;

public class MiniDaoBeanScannerConfig implements BeanDefinitionRegistryPostProcessor {
	private String basePackages;
	private boolean showSql;
	private Class<? extends Annotation> miniDaoAnnotation = MiniDao.class;
	
	public void postProcessBeanFactory(ConfigurableListableBeanFactory arg0)
			throws BeansException {
		// TODO Auto-generated method stub
		
	}

	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry arg0)
			throws BeansException {
		// TODO Auto-generated method stub
		 registerProxyHandler(arg0);
		 MiniDaoClassPathBeanScanner scanner = new MiniDaoClassPathBeanScanner(arg0, miniDaoAnnotation);
		 scanner.doScan(basePackages);		
	}
	
	private void registerProxyHandler(BeanDefinitionRegistry registry) {
		//����ע��
		GenericBeanDefinition genericBeanDefinition = new GenericBeanDefinition();
		genericBeanDefinition.setBeanClass(MiniDaoHandler.class);
		genericBeanDefinition.getPropertyValues().add("showSql", showSql);
		registry.registerBeanDefinition("miniDaoHandler", genericBeanDefinition);
		
	}
	
	public String getBasePackages() {
		return basePackages;
	}
	
	public void setBasePackages(String basePackages) {
		this.basePackages = basePackages;
	}
	
	public boolean isShowSql() {
		return showSql;
	}
	
	public void setShowSql(boolean showSql) {
		this.showSql = showSql;
	}
	
	public Class<? extends Annotation> getMiniDaoAnnotation() {
		return miniDaoAnnotation;
	}
	
	public void setPreDaoAnnotation(Class<? extends Annotation> miniDaoAnnotation) {
		this.miniDaoAnnotation = miniDaoAnnotation;
	}	

}
